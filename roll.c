#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

/* Default sided dice */
#define DEFAULT   6
/* What we cannot roll */
const int badlist[] = { 0, 1 };

int roll(int num)
{
    int dice = (rand() % num);
    dice += 1;
    return dice;
}

int isvalbanned(int val)
{
    int s = sizeof(badlist);
    for(int i=0; i<s; ++i)
    {
        if (badlist[i] == val)
        {
            return 1;
        }
    }
    return 0;
}

int main(int argc, char *argv[])
{
    /* "Random" seed */
    srand(time(NULL));
    int diceresult;

    if (argc > 1)
    {
        for (int i = 1; i<argc; ++i)
        {
            if (atoi(argv[i]) == 0)
            {
                printf("[%s] is not a number we can roll\n", argv[i]);
                continue;
            }

            int x = atoi(argv[i]);
            if (isvalbanned(x))
            {
                printf("Rolling a [%d] sided dice is not allowed!\n", x);
                continue;
            }
            else
            {
                printf("Rolling [%d]: ", x);
                diceresult = roll(atoi(argv[i]));
                printf("%d\n", diceresult);
            }
        }
    }
    else
    {
        printf("Rolling [%d]: ", DEFAULT);
        diceresult = roll(DEFAULT);
        printf("%d\n", diceresult);
    }
}
